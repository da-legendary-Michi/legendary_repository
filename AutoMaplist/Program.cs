﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreAutoDownloader;
using System.Configuration;

namespace AutoMaplist
{
    class Program
    {
        static private ManualInputCredential MyCredential = null;
        static private Dictionary<string, string> Parameters = null;
        static bool reWrotePassword = false;

        static void Main(string[] args)
        {
            if(args.Length>0)
                WriteSettings(args);
            StartWork(args);
            CoreFunctions.ReleaseRessource();
        }

        private static void WriteSettings(string[] args)
        {
            try
            {
                Parameters = GetParameterList(args);

                //foreach(var entry in Parameters)
                //    Console.WriteLine(entry.Key + " " + entry.Value);

                if (Parameters.Keys.Contains("-h") || Parameters.Keys.Contains("-help"))
                {
                    Console.WriteLine(@"Program accept following Inputs
                                        -user=              SFTP-User
                                        -password=          SFTP-Password
                                        -url=               Server-URL
                                        -port=              SFTP-Port*
                                        -keyfile=           Path to PK-File*
                                        -keyfilepassword=   Password for PK-File*
                                        -key=               PK-Key as string*
                                        -h / help=          shows this*
                                        -path=              Directory Path of SFTP for reverse seek default is login directory*
                                        -filename=          Name of the output File*
                                        -outputextension=   Extension name of the output File*
                                        -outputpath=        output path for maplist*
                                        -removefromstart=   You can insert a Path here to be removed from the final list entries
                                        ");
                }

                if(Parameters["password"]!= string.Empty)
                {
                    Console.WriteLine("Hey mate you entered an Password trough Bash, it maybe gets escaped (problems with specialsigns)." + Environment.NewLine + "Input it again? y/n?");

                    Console.WriteLine("Write y or yes if you are sure your code has escape problems, write n or no to continue!" + Environment.NewLine);

                    string choice = Console.ReadLine();

                    switch (choice)
                    {
                        case "y":
                        case "yes":
                            choice = "y";
                            break;
                        case "n":
                        case "no":
                            choice = "n";
                            break;
                        default:
                            break;
                    }

                    //char inputYesNo = Console.ReadKey().KeyChar;
                    if (choice == "y")
                    {
                        Console.WriteLine(Environment.NewLine + "Okay, lets try. Type it in again and press than enter...");
                        string newPassword = Console.ReadLine();
                        Parameters.Remove("password");
                        Parameters.Add("password", newPassword);
                        reWrotePassword = true;
                    }

                }
                string filename = "maplist";
                string path = string.Empty;
                string outputPath = Directory.GetCurrentDirectory();
                string outputExtension = ".txt";
                string removeFromStart = string.Empty;

                if (!Parameters.TryGetValue("filename", out filename))
                {
                    Parameters.Add("filename", "maplist");
                };
                if (!Parameters.TryGetValue("path", out path))
                {
                    Parameters.Add("path", string.Empty);
                };
                if (!Parameters.TryGetValue("outputpath", out outputPath))
                {
                    Parameters.Add("outputpath", Directory.GetCurrentDirectory().TrimEnd('\\') + @"\");
                };
                if (!Parameters.TryGetValue("outputextension", out outputExtension))
                {
                    Parameters.Add("outputextension", ".txt");
                };


                //if (Parameters.Keys.Contains("-path"))
                //    path = Parameters.FirstOrDefault(x => x.Key == "-path").Value;
                //if (Parameters.Keys.Contains("-filename"))
                //    filename = Parameters.FirstOrDefault(x => x.Key == "-filename").Value;
                //if (Parameters.Keys.Contains("-outputpath"))
                //    outputPath = Parameters.FirstOrDefault(x => x.Key == "-outputpath").Value;
                //if (Parameters.Keys.Contains("-outputextension"))
                //    outputExtension = Parameters.FirstOrDefault(x => x.Key == "-outputextension").Value;
                //if (Parameters.Keys.Contains("-removefromstart"))
                //    removeFromStart = Parameters.FirstOrDefault(x => x.Key == "-removefromstart").Value;

                //outputPath.TrimEnd('\\');
                //outputPath = outputPath + @"\";

                //if (!string.IsNullOrWhiteSpace(removeFromStart)) removeFromStart = removeFromStart.Replace("//", "/");

                if(Parameters.Keys.Contains("outputpath"))
                {
                    Parameters["outputpath"] = Parameters["outputpath"].TrimEnd('\\') + '\\';
                }

                foreach (var entry in Parameters)
                {
                    AddUpdateAppSettings(entry.Key.TrimStart('-'), entry.Value);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static void ReadSetting(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string result = appSettings[key] ?? "Not Found";
                Console.WriteLine(result);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error reading app settings");
            }
        }

        private static void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
        }

        private static bool StartWork(string[] args)
        {
            try
            {
                StringBuilder outputText = new StringBuilder();

                //TODO: Ersetzen bzw. zweite Methode für GetCredentialsFromAppsettings
                //MyCredential = CoreFunctions.ManualInputCredentialFromFromArgs(args);
                if(reWrotePassword)
                    Console.WriteLine("Because you rewrote the password, initializing will run over the appsettings not console arguments");

                if(args.Length>0 && reWrotePassword == false)
                    MyCredential = CoreFunctions.ManualInputCredentialFromFromArgs(args);
                else
                    MyCredential = CoreFunctions.ManualInputCredentialFromNameValueCollection(ConfigurationManager.AppSettings);

                var maps = CoreFunctions.GetMapList((ManualInputCredential)MyCredential, ConfigurationManager.AppSettings["path"]);

                List<string> sortedMaps = maps.OrderBy(x => x.Substring(x.LastIndexOf('/'), (x.Substring(x.LastIndexOf('/')).IndexOf('_') + x.LastIndexOf('/') ) - x.LastIndexOf('/'))).ThenBy(x => x.Substring(x.LastIndexOf('/'))).ToList();//
                    
                  //  /.GroupBy(mode => mode.Substring(0, mode.IndexOf('_')));
                if(sortedMaps != null && sortedMaps.Count > 0)
                {
                    var test = ConfigurationManager.AppSettings["removefromstart"].Replace("//", "/");
                    foreach (var entry in sortedMaps)
                    {
                        string entryToWrite = entry.Substring(0,entry.LastIndexOf('.'));
                        
                        if (ConfigurationManager.AppSettings["removefromstart"] == string.Empty)
                        {
                            outputText.AppendLine(entryToWrite);
                        }
                        else
                            outputText.AppendLine(entryToWrite.Remove(entryToWrite.IndexOf(@ConfigurationManager.AppSettings["removefromstart"].Replace("//", "/")),
                                                                @ConfigurationManager.AppSettings["removefromstart"].Replace("//", "/").Length));
                    }

                    foreach(string key in ConfigurationManager.AppSettings)
                    {
                        Console.WriteLine($"{key} = {ConfigurationManager.AppSettings[key]}");
                    }



                    File.WriteAllText(ConfigurationManager.AppSettings["outputpath"] + ConfigurationManager.AppSettings["filename"] + ConfigurationManager.AppSettings["outputextension"], outputText.ToString());
                }
                Console.WriteLine($"Wrote List with {sortedMaps.Count} as {ConfigurationManager.AppSettings["outputpath"] + ConfigurationManager.AppSettings["filename"] + ConfigurationManager.AppSettings["outputextension"] }");

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        private static Dictionary<string, string> GetParameterList(string[] args)
        {
            Dictionary<string, string> tmp = new Dictionary<string, string>();
            for (int i = 0; i < args.Length; i++)
            {
                tmp.Add(@args[i].Substring(0, args[i].IndexOf('=')).TrimStart('-'), (@args[i].Substring(args[i].IndexOf('=') + 1)));
            }
            return tmp;
        }


    }
}
