﻿using PriorityForcer.Extensions;
using PriorityForcer.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace PriorityForcer.Cacher
{
    /// <summary>
    /// MemoryCacher class, inherits Interface ICacher and class MemoryCache
    /// </summary>
    public class MemoryCacher :  MemoryCache , ICacher
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="config"></param>
        public MemoryCacher(string name, NameValueCollection config = null) : base(name, config)
        {
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="config"></param>
        /// <param name="ignoreConfigSection"></param>
        public MemoryCacher(string name, NameValueCollection config, bool ignoreConfigSection) : base(name, config, ignoreConfigSection)
        {
        }
        /// <summary>
        /// Add or Get Existing Object
        /// </summary>
        /// <typeparam name="T">Type of Data</typeparam>
        /// <param name="key">Key, Index</param>
        /// <param name="data">concrecte data</param>
        /// <param name="absoluteExpiration">Expiration Offset for cacheFile</param>
        /// <returns></returns>
        public T AddOrGetExisting<T>(string key, T data, DateTimeOffset absoluteExpiration)
        {
            try
            {
                return (T)base.AddOrGetExisting(key, data, absoluteExpiration);
            }
            catch (Exception ex)
            {
                ex.Log();
                return default;
            }
        }
        /// <summary>
        /// Exists cachefile with <paramref name="key"/> in Cache?
        /// </summary>
        /// <param name="key">Key, Index</param>
        /// <returns></returns>
        public bool Contains(string key)
        {
            try
            {
                return base.Contains(key);
            }
            catch (Exception ex)
            {
                ex.Log();
                return false;
            }
        }
        /// <summary>
        /// Get CacheItem with <paramref name="key"/>
        /// </summary>
        /// <typeparam name="T">Type of CacheItem</typeparam>
        /// <param name="key">Key, Index</param>
        /// <returns></returns>
        public T GetCacheItem<T>(string key)
        {
            try
            {
                return (T)(object)base.GetCacheItem(key);
            }
            catch (Exception ex)
            {
                ex.Log();
                return default;
            }
        }
        /// <summary>
        /// Remove Cache-File with <paramref name="key"/>
        /// </summary>
        /// <typeparam name="T">Type of CacheItem</typeparam>
        /// <param name="key">Key, Index</param>
        /// <param name="reason">RemoveReason</param>
        /// <returns></returns>
        public T Remove<T>(string key, CacheEntryRemovedReason reason)
        {
            try
            {
                return (T)base.Remove(key, reason: reason, null);
            }
            catch (Exception ex)
            {
                ex.Log();
                return default;
            }
        }
        /// <summary>
        /// Set CacheFile with <paramref name="key"/> and <paramref name="data"/> in Cache, <paramref name="absoluteExpiration"/> sets how long it should be kept in Cache
        /// </summary>
        /// <typeparam name="T">Type of CacheItem</typeparam>
        /// <param name="key">Key, Index</param>
        /// <param name="data">CacheItem</param>
        /// <param name="absoluteExpiration">DateTimeOffset for CacheItem expiration</param>
        public void Set<T>(string key, T data, DateTimeOffset absoluteExpiration)
        {
            try
            {
                base.Set(key, data, absoluteExpiration);
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
    }
}
