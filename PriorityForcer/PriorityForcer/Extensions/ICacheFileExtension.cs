﻿using PriorityForcer.Interfaces;
using System.IO;

namespace PriorityForcer.Extensions
{
    /// <summary>
    /// ICacheFileExtension including singleton to save ICacher Instance
    /// </summary>
    public static class ICacheFileExtension
    {
        /// <summary>
        /// Singleton Class
        /// </summary>
        public partial class Singleton
        {
            /// <summary>
            /// Effective Singleton
            /// </summary>
            public static Singleton Instance { get; set; } = new Singleton();
            private Singleton()
            {
            }
            /// <summary>
            /// Reference for object of ICacher Type
            /// </summary>
            public ICacher Cacher { get; set; }
        }
        /// <summary>
        /// Get and Remove Cache-File
        /// </summary>
        /// <typeparam name="T">ICacheFile</typeparam>
        /// <param name="data">data</param>
        /// <returns></returns>
        public static T GetAndRemove<T>(this T data) where T : ICacheFile , new()
        {
            try
            {
                ICacher cacher = Singleton.Instance.Cacher;
                return cacher.Remove<T>(data.Key, System.Runtime.Caching.CacheEntryRemovedReason.Removed);
            }
            catch (System.Exception ex)
            {
                ex.Log();
                return default;
            }
        }
        /// <summary>
        /// Just Get Cache-File (and don't remove)
        /// </summary>
        /// <typeparam name="T">ICacheFile</typeparam>
        /// <param name="data">CacheFile</param>
        /// <returns></returns>
        public static T Get<T>(this T data) where T : ICacheFile, new()
        {
            try
            {
                ICacher cacher = Singleton.Instance.Cacher;
                return cacher.GetCacheItem<T>(data.Key);
            }
            catch (System.Exception ex)
            {
                ex.Log();
                return default;
            }
        }
        /// <summary>
        /// Add or Get Cache-File
        /// </summary>
        /// <typeparam name="T">ICacheFile</typeparam>
        /// <param name="data">CacheFile</param>
        /// <returns></returns>
        public static T AddOrGet<T>(this T data) where T : ICacheFile, new()
        {
            try
            {
                ICacher cacher = Singleton.Instance.Cacher;
                return cacher.AddOrGetExisting<T>(data.Key,data,System.DateTimeOffset.Now.AddMinutes(10));
            }
            catch (System.Exception ex)
            {
                ex.Log();
                return default;
            }
        }
        /// <summary>
        /// Set Cache-File
        /// </summary>
        /// <typeparam name="T">ICacheFile</typeparam>
        /// <param name="data">CacheFile</param>
        public static void Set<T>(this T data) where T : ICacheFile, new()
        {
            try
            {
                ICacher cacher = Singleton.Instance.Cacher;
                cacher.Set(data.Key, data, System.DateTimeOffset.Now.AddMinutes(10));
            }
            catch (System.Exception ex)
            {
                ex.Log();
            }
        }
    }
}
