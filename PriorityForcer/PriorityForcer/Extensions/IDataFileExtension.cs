﻿using PriorityForcer.Interfaces;
using System.IO;

namespace PriorityForcer.Extensions
{
    /// <summary>
    /// IDataFileExtension including singleton to save ISerializer Instance
    /// </summary>
    public static class IDataFileExtension
    {
        /// <summary>
        /// Singleton Class
        /// </summary>
        public partial class Singleton
        {
            /// <summary>
            /// Effective Singleton
            /// </summary>
            public static Singleton Instance { get; set; } = new Singleton();
            private Singleton()
            {
            }
            /// <summary>
            /// Reference for object of ISerializer Type
            /// </summary>
            public ISerializer Serializer { get; set; }
        }
        /// <summary>
        /// Save IDataFile <paramref name="data"/> (or create new) to path
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">Extension Param</param>
        /// <param name="path">Path to save data to</param>
        public static void Save<T>(this T data, string path) where T : IDataFile, new()
        {
            try
            {
                if (!File.Exists(path))
                    CreateNew<T>(path);
                using (FileStream stream = new FileStream(path, FileMode.OpenOrCreate))
                {
                    ISerializer serializer = Singleton.Instance.Serializer;
                    serializer.Serialize(data, stream);
                }
            }
            catch (System.Exception ex)
            {
                ex.Log();
            }
        }
        /// <summary>
        /// Loads or Creates IDataFile from/at path
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">Extension Param</param>
        /// <param name="path">Path to load data from</param>
        /// <returns></returns>
        public static T Load<T>(this T data, string path) where T : IDataFile, new()
        {
            try
            {
                if (!File.Exists(path))
                    return CreateNew<T>(path);
                using (FileStream stream = new FileStream(path, FileMode.Open))
                {
                    ISerializer serializer = Singleton.Instance.Serializer;
                    return (T)serializer.Deserialize<T>(stream);
                }
            }
            catch (System.Exception ex)
            {
                ex.Log();
                return new T();
            }
        }
        /// <summary>
        /// Creates new IDataFile <typeparamref name="T"/> at path
        /// </summary>
        /// <typeparam name="T">IDataFile</typeparam>
        /// <param name="path">path to create File at</param>
        /// <returns></returns>
        private static T CreateNew<T>(string path) where T : IDataFile, new()
        {
            try
            {
                var data = new T();
                using (FileStream stream = new FileStream(path, FileMode.OpenOrCreate))
                {
                    ISerializer serializer = Singleton.Instance.Serializer;
                    serializer.Serialize(data, stream);
                    return data;
                }
            }
            catch (System.Exception ex)
            {
                ex.Log();
                return new T();
            }
        }
    }
}
