﻿using PriorityForcer.Helper;
using System;

namespace PriorityForcer.Extensions
{
    /// <summary>
    /// ExceptionExtension includes to logging Helpful Extensions
    /// for this Exception and this string
    /// </summary>
    public static class ExceptionExtension
    {
        /// <summary>
        /// DisableLogging at all, default is false
        /// </summary>
        public static bool DisableLogging = false;
        /// <summary>
        /// OnlyExceptionLogging, default is false
        /// </summary>
        public static bool OnlyExceptionLogging = false;
        /// <summary>
        /// Logs Exception
        /// </summary>
        /// <param name="exception">this Exception</param>
        /// <param name="logToConsole">default=true</param>
        /// <param name="logToTextfile">default=true</param>
        public static void Log(this Exception exception, bool logToConsole=true, bool logToTextfile=true)
        {
            if (DisableLogging) return;
            if(logToConsole)
                Console.WriteLine(exception.ToString());
            if(logToTextfile)
            {
                LogWriter.LogWrite(exception.ToString());
            }
        }
        /// <summary>
        /// Logs String
        /// </summary>
        /// <param name="string">this string</param>
        /// <param name="logToConsole">default=true</param>
        /// <param name="logToTextfile">default=true</param>
        public static void Log(this string @string, bool logToConsole = true, bool logToTextfile = true)
        {
            if (DisableLogging) return;
            
            if (logToConsole)
                Console.WriteLine(@string.ToString());
            if (OnlyExceptionLogging) return;
            if (logToTextfile)
            {
                LogWriter.LogWrite(@string.ToString());
            }
        }
    }
}
