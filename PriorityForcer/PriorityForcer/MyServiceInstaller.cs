﻿using PriorityForcer.Extensions;
using System;
using System.Collections;
using System.ComponentModel;
using System.ServiceProcess;

namespace PriorityForcer
{
    /// <summary>
    /// Project Installer for adding Service to Windowssystem
    /// </summary>
    [RunInstaller(true)]
    public partial class MyProjectInstaller : System.Configuration.Install.Installer
    {
        private ServiceInstaller serviceInstaller;
        private ServiceProcessInstaller serviceProcessInstaller;
        /// <summary>
        /// This all is necessary for the Windows Setup Project!
        /// </summary>
        public MyProjectInstaller()
        {
            InitializeComponent();
            try
            {
                //Instantiate installers for process and Service
                serviceProcessInstaller = new ServiceProcessInstaller();
                serviceInstaller = new ServiceInstaller();

                //UAC
                serviceProcessInstaller.Account = ServiceAccount.LocalSystem;

                //Starttype for service
                serviceInstaller.StartType = ServiceStartMode.Manual;

                serviceInstaller.ServiceName = "PriorityForcingService";

                this.Installers.AddRange(new System.Configuration.Install.Installer[] {
                this.serviceInstaller,
                this.serviceProcessInstaller});
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
        /// <summary>
        /// Override of Installer.Install method to remove old service via cmd.exe command and implement logging
        /// </summary>
        /// <param name="savedState"></param>
        public override void Install(IDictionary savedState)
        {
            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.UseShellExecute = true;
                startInfo.Verb = "runas";
                string processDelete = $"sc.exe delete \"{serviceInstaller.ServiceName}\" & exit";

                $"run cmd.exe as admin with following action: {processDelete}".Log();
                startInfo.Arguments = processDelete;
                process.StartInfo = startInfo;
                process.Start();
                process.WaitForExit(2500);

                base.Install(savedState);
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
        /// <summary>
        /// Override of Installer.Uninstall with implemented logging
        /// </summary>
        /// <param name="savedState"></param>
        public override void Uninstall(IDictionary savedState)
        {
            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.UseShellExecute = true;
                startInfo.Verb = "runas";
                string processDelete = $"sc.exe delete \"{serviceInstaller.ServiceName}\" & exit";

                $"run cmd.exe as admin with following action: {processDelete}".Log();
                startInfo.Arguments = processDelete;
                process.StartInfo = startInfo;
                process.Start();
                process.WaitForExit(2500);
                base.Uninstall(savedState);
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
        /// <summary>
        /// Override of Installer.Rollback with implemented logging
        /// </summary>
        /// <param name="savedState"></param>
        public override void Rollback(IDictionary savedState)
        {
            try
            {
                base.Rollback(savedState);
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
        /// <summary>
        /// Override of Installer.Commit with implemented logging
        /// </summary>
        /// <param name="savedState"></param>
        public override void Commit(IDictionary savedState)
        {
            try
            {
                base.Commit(savedState);
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
    }
}
