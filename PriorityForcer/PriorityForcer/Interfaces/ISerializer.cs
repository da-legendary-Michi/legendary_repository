﻿using System.IO;
namespace PriorityForcer.Interfaces
{
    /// <summary>
    /// Interface for simple Serializer Injection
    /// </summary>
    public interface ISerializer
    {
        /// <summary>
        /// Deserialize Stream to T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
         T Deserialize<T>(Stream stream);
        /// <summary>
        /// Serialize T data to Stream
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="stream"></param>
         void Serialize<T>(T data, Stream stream);
    }
}
