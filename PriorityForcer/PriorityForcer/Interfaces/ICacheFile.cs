﻿namespace PriorityForcer.Interfaces
{
    /// <summary>
    /// Interface for Extension (Caching)
    /// </summary>
    public interface ICacheFile
    {
        /// <summary>
        /// ICacheFile needs a Key -> Index
        /// </summary>
         string Key { get; }
    }
}
