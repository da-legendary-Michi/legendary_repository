﻿using System;
using System.IO;
using System.Runtime.Caching;

namespace PriorityForcer.Interfaces
{
    /// <summary>
    /// Interface for simple Caching Injection
    /// </summary>
    public interface ICacher
    {
        /// <summary>
        /// Remove entry from Cache
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="key">Key for Cachefile</param>
        /// <param name="reason"></param>
        /// <returns></returns>
        T Remove<T>(string key, CacheEntryRemovedReason reason);
        /// <summary>
        /// Add or Get Existing
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="key">Key for Cachefile</param>
        /// <param name="data">T</param>
        /// <param name="absoluteExpiration">DateTimeOffset</param>
        /// <returns></returns>
        T AddOrGetExisting<T>(string key, T data, DateTimeOffset absoluteExpiration);
        /// <summary>
        /// GetCacheItem
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="key">Key for Cachefile</param>
        /// <returns></returns>
        T GetCacheItem<T>(string key);
        /// <summary>
        /// Set
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="key">Key for Cachefile</param>
        /// <param name="data">T</param>
        /// <param name="absoluteExpiration">DateTimeOffset</param>
        void Set<T>(string key, T data, DateTimeOffset absoluteExpiration);
        /// <summary>
        /// If Cache contains value behind key
        /// </summary>
        /// <param name="key">key for Cacheitem</param>
        /// <returns>Exists</returns>
        bool Contains(string key);
    }
}
