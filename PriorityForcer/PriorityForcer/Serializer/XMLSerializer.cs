﻿using PriorityForcer.Extensions;
using PriorityForcer.Interfaces;
using System.IO;
using System.Xml.Serialization;

namespace PriorityForcer.Serializer
{
    /// <summary>
    /// Simple implemented XMLSerializer without Datacontracts
    /// </summary>
    public class XMLSerializer : ISerializer
    {
        /// <summary>
        /// Deserialize stream to object of Type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public T Deserialize<T>(Stream stream)
        {
            var serializer = new XmlSerializer(typeof(T));
            return (T)serializer.Deserialize(stream);
        }
        /// <summary>
        /// Serialize object of Type to stream
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="stream"></param>
        public void Serialize<T>(T data, Stream stream)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stream, data);
            }
            catch (System.Exception ex)
            {
                ex.Log();
            }
        }
    }
}
