﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityForcer.Enums
{
    /// <summary>
    /// Is maybe obsolete, Mirror of ThreadPriorityLevel Enum in .Net
    /// https://docs.microsoft.com/de-de/dotnet/api/system.runtime.caching.memorycache?view=netframework-4.8
    /// AboveNormal = 1,
    /// BelowNormal = -1,
    /// Highest = 2,
    /// Idle = -15,
    /// Lowest = -2,
    /// Normal = 0,
    /// TimeCritical = 15
    /// </summary>
    public enum ThreadPriorityLevel
    {
        #pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        AboveNormal = 1,
        BelowNormal = -1,
        Highest = 2,
        Idle = -15,
        Lowest = -2,
        Normal = 0,
        TimeCritical = 15
        #pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
