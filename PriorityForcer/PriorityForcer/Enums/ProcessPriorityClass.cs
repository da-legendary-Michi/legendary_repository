﻿namespace PriorityForcer.Enums
{
    /// <summary>
    /// Is maybe obsolete, Mirror of ProcessPriorityClass Enum in .Net
    /// https://docs.microsoft.com/de-de/dotnet/api/system.diagnostics.processpriorityclass?view=netframework-4.8
    ///    Normal = 32,
    ///    Idle = 64,
    ///    High = 128,
    ///    RealTime = 256,
    ///    BelowNormal = 16384,
    ///    AboveNormal = 32768
    /// </summary>
    public enum ProcessPriorityClass
    {
        #pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        Normal = 32,
        Idle = 64,
        High = 128,
        RealTime = 256,
        BelowNormal = 16384,
        AboveNormal = 32768
        #pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}

