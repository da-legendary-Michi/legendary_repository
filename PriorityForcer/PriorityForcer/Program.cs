﻿using PriorityForcer.BusinessObjects;
using PriorityForcer.Cacher;
using PriorityForcer.Extensions;
using PriorityForcer.Helper;
using PriorityForcer.Interfaces;
using PriorityForcer.Serializer;
using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Caching;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using ProcessPriorityClass = System.Diagnostics.ProcessPriorityClass;

namespace PriorityForcer
{
    /// <summary>
    /// Main Class
    /// </summary>
    /// 
    [RunInstaller(true)]
    internal static class Program
    {
        /// <summary>
        /// const ServiceName
        /// </summary>
        public const string ServiceName = "PriorityForcingService";
        /// <summary>
        /// static PollTimer
        /// </summary>
        public static System.Timers.Timer PollTimer;
        /// <summary>
        /// static Config
        /// </summary>
        public static Config Config;
        /// <summary>
        /// nested ServiceClass if called as Service
        /// </summary>
        public partial class PriorityForcingService : ServiceBase
        {
            public PriorityForcingService()
            {
                //InitializeComponent();
                ServiceName = Program.ServiceName;
            }
            /// <summary>
            /// calls Program OnStart method for consoleapp compatibility
            /// </summary>
            /// <param name="args"></param>
            protected override void OnStart(string[] args)
            {
                try
                {
                    Program.OnStart();
                }
                catch (Exception ex)
                {
                    ex.Log();
                }
            }
            /// <summary>
            /// calls Program OnStop method for consoleapp compatibility
            /// </summary>
            protected override void OnStop()
            {
                try
                {
                    Program.OnStop();
                }
                catch (Exception ex)
                {
                    ex.Log();
                }
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            try
            {
                if (!Static_Methods.Core_Methods.IsUserAdministrator())
                {
                    "WARNING: Service is not started as admin, can't access running services. Exitcode 1.".Log();
                    Environment.Exit(1);
                }
                Initialize();

                if (!Environment.UserInteractive)
                {
                    "Started as service".Log();
                    using (var priorityForcingService = new PriorityForcingService())
                        ServiceBase.Run(priorityForcingService);
                }
                else
                {
                    "Started as ConsoleApp".Log();
                    OnStart();

                    Console.WriteLine("Press any key to stop the Service");
                    Console.ReadKey(true);

                    OnStop();
                }
            }
            catch (Exception ex)
            {
                ex.Log();
            }

        }
        /// <summary>
        /// Initializes Config , starts Timer, saves Config and forces Processes to setted PriorityClass value 
        /// </summary>
        private static void Initialize()
        {
            try
            {
                string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                IDataFileExtension.Singleton.Instance.Serializer = new XMLSerializer();
                ICacheFileExtension.Singleton.Instance.Cacher = new MemoryCacher("ProcessCache");

                Config = Config.Load($"{path.TrimEnd('\\')}\\config.xml");
                if (Config == null)
                    Config = new Config();
                ExceptionExtension.DisableLogging = Config.SettingConfig.DisableLogging;
                ExceptionExtension.OnlyExceptionLogging = Config.SettingConfig.OnlyExceptionLogging;

                $"Config loaded or created. ({$"{path.TrimEnd('\\')}\\config.xml"})".Log();
                PollTimer = new System.Timers.Timer(Convert.ToDouble(Config.SettingConfig.pollTimeInMilliseconds));
                $"Timer value= {Config.SettingConfig.pollTimeInMilliseconds}".Log();
                PollTimer.AutoReset = true;
                PollTimer.Elapsed -= elapsedEventHandler;
                PollTimer.Elapsed += elapsedEventHandler;

                RefreshProcessConfig();

                "Saving Config".Log();
                Config.Save($"{path.TrimEnd('\\')}\\config.xml");

                "Initial run (first run before Timer run starts)".Log();
                ForcingAssignedPriority();
                PollTimer.Enabled = true;
                PollTimer.Start();
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
        /// <summary>
        /// OnStart method Enabling Timer
        /// </summary>
        private static void OnStart()
        {
            "OnStart - Routine started".Log();
            try
            {
                Initialize();
                PollTimer.Enabled = true;
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
        /// <summary>
        /// OnStop method Disables Timer
        /// </summary>
        private static void OnStop()
        {
            "OnStop - Routine ended".Log();
            try
            {
                PollTimer.Enabled = false;
                PollTimer.Stop();
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
        /// <summary>
        /// Eventhandler for elapsed Timer
        /// </summary>
        /// <param name="sender">PollTimer</param>
        /// <param name="e">ElapsedEventArgs</param>
        private static void elapsedEventHandler(object sender, ElapsedEventArgs e)
        {
            try
            {
                "Timer event to manage PriorityClasses of Processes starts".Log();
                "Clear MemoryCache".Log();
                ((MemoryCache)ICacheFileExtension.Singleton.Instance.Cacher).Trim(100);
                ForcingAssignedPriority();
                "Timer event to manage PriorityClasses of Processes has ended".Log();
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
        /// <summary>
        /// Refreshs and saves Configfile to programdirectory
        /// </summary>
        private static void RefreshProcessConfig()
        {
            "Refreshing ProcessList for Config".Log();
            try
            {
                var processArray = System.Diagnostics.Process.GetProcesses();
                for (int i = 0; i < processArray.Length; i++)
                {
                    try
                    {
                        var entryInArray = processArray[i];
                        if (entryInArray is Process process)
                        {
                            if ((process.Id != 0 && process.Id != 4) && (process.MachineName == Environment.MachineName || process.MachineName == ".") && (process.HasExited != true))
                            {
                                $"Process found. Name {process.ProcessName} , PriorityClass {process.PriorityClass} , BasePriority {process.BasePriority} , BoostEnabled {process.PriorityBoostEnabled}".Log();

                                if (!Config.ProcessList.Exists(x => x.Name == process.ProcessName))
                                {
                                    Config.ProcessList.Add(new ProcessConfig { ProcessPriorityClass = (Enums.ProcessPriorityClass)Enum.ToObject(typeof(Enums.ProcessPriorityClass), process.PriorityClass), Name = process.ProcessName });
                                    "Successfully added a new process".Log();
                                }
                                else
                                {
                                    "ProcessName exists already in Config".Log();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.Log();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
        /// <summary>
        /// Forces Priorities to Processes
        /// </summary>
        private static void ForcingAssignedPriority()
        {
            "For every process from config with ignore=false start to force assign ProcessPriorityClass".Log();
            try
            {
                foreach (var processToProcess in Config.ProcessList.Where(x => x.Ignore == false))
                {
                    var processArray = System.Diagnostics.Process.GetProcessesByName(processToProcess.Name);
                    if (processToProcess.KillProcess)
                    {
                        try
                        {
                            for (int i = 0; i < processArray.Length; i++)
                            {
                                processArray[i].Kill();

                            }
                            $"Kill Signal for {processToProcess.Name} send.".Log();
                        }
                        catch (Exception ex)
                        {
                            ex.Log();
                        }
                    }
                    else
                    {
                        for (int i = 0; i < processArray.Length; i++)
                        {
                            if (!processToProcess.IgnoreForcingThreadPriority)
                            {
                                processToProcess.Set();
                                "Added ProcessConfig with active and IgnoreForcingThreadPriority false and ForceThreadPriority to MemoryCache for BackgroundThread processing".Log();
                            }

                            try
                            {
                                var entryInArray = processArray[i];
                                if (entryInArray is Process process)
                                {
                                    if ((process.Id != 0 && process.Id != 4) && (process.MachineName == Environment.MachineName || process.MachineName == ".") && (process.HasExited != true))
                                    {
                                        if (process.ProcessName == processToProcess.Name && process.PriorityClass != (ProcessPriorityClass)processToProcess.ProcessPriorityClass && process.PriorityClass != ProcessPriorityClass.Idle)
                                        {
                                            process.PriorityClass = (ProcessPriorityClass)processToProcess.ProcessPriorityClass;
                                            $"force assigned PriorityClass: {(ProcessPriorityClass)processToProcess.ProcessPriorityClass} to: {process.ProcessName}".Log();
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Log();
                            }
                        }
                    }

                    if (((MemoryCache)ICacheFileExtension.Singleton.Instance.Cacher).Contains(processToProcess.Name))
                    {

                        ThreadPool.QueueUserWorkItem(RunProcessThreadProcessing, processToProcess);
                        $"Added {processToProcess.Name} BackgroundThread for enforcing ThreadLevel".Log();
                    }
                }
            }
            catch (Win32Exception ex)
            {
                ex.Log();
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
        private static void RunProcessThreadProcessing(object state)
        {
            try
            {
                if (state is ProcessConfig activeProcess)
                {

                    var cacheFile = activeProcess.GetAndRemove();
                    $"Removed processConfig from Cache: {cacheFile.Name}".Log();
                    var processArray = System.Diagnostics.Process.GetProcessesByName(activeProcess.Name);
                    for (int i = 0; i < processArray.Length; i++)
                    {
                        try
                        {
                            var process = processArray[i];
                            var processThreads = process.Threads;
                            foreach (var thread in processThreads)
                            {
                                if (thread is ProcessThread processThread)
                                {
                                    if (processThread.PriorityLevel != (ThreadPriorityLevel)activeProcess.ThreadPriorityLevel)
                                    {
                                        processThread.PriorityLevel = (ThreadPriorityLevel)activeProcess.ThreadPriorityLevel;
                                        $"Set {cacheFile.Name} Thread to {processThread.PriorityLevel}".Log();
                                    }
                                    processThread?.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Log();
                        }
                    }
                    $"BackgroundThread for {activeProcess.Name} is now finished.".Log();
                }
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
    }
}
