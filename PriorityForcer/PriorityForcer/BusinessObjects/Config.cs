﻿using PriorityForcer.Interfaces;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PriorityForcer.BusinessObjects
{
    /// <summary>
    /// Config file that provides Necessary settings for PriorityChanger
    /// </summary>
    [DataContract]
    public class Config : IDataFile
    {
        /// <summary>
        /// Stores Process and makes them manipulateable in xml
        /// </summary>
        [DataMember]
        public List<ProcessConfig> ProcessList { get; set; } = new List<ProcessConfig>();
        /// <summary>
        /// Stores in XML the PriorityChangerService Applicationsettings
        /// </summary>
        [DataMember]
        public SettingConfig SettingConfig { get; set; } = new SettingConfig();

        [OnDeserialized]
        internal void OnSerializingMethod(StreamingContext context)
        {
            if (ProcessList == null)
            {
                ProcessList = new List<ProcessConfig>();
            }
            if (SettingConfig == null)
            {
                SettingConfig = new SettingConfig();
            }
        }
    }
}
