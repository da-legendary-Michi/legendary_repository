﻿using System.Runtime.Serialization;

namespace PriorityForcer.BusinessObjects
{
    /// <summary>
    /// General Settings
    /// </summary>
    [DataContract]
    public class SettingConfig
    {
        /// <summary>
        /// Polltime in Milliseconds Default = 600 000 (10 minutes)
        /// </summary>
        [DataMember]
        public string pollTimeInMilliseconds = @"600000";
        /// <summary>
        /// Disable Console and Logfile logging for PriorityForcer runtime
        /// </summary>
        [DataMember]
        public bool DisableLogging = false;
        /// <summary>
        /// Enable Only ExceptionLogging, not works if <DisableLogging>true</DisableLogging>
        /// </summary>
        [DataMember]
        public bool OnlyExceptionLogging = false;
    }
}