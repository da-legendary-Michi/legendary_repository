﻿using PriorityForcer.Enums;
using PriorityForcer.Interfaces;
using System.Runtime.Serialization;

namespace PriorityForcer.BusinessObjects
{
    /// <summary>
    /// ProcessSettings
    /// </summary>
    [DataContract]
    public class ProcessConfig : ICacheFile
    {
        /// <summary>
        /// Process Name
        /// </summary>
        [DataMember]
        public string Name = @"csgo.exe";
        /// <summary>
        /// ProcessPriorityClass see msdn https://docs.microsoft.com/de-de/dotnet/api/system.diagnostics.processpriorityclass?view=netframework-4.8
        /// </summary>
        [DataMember]
        public ProcessPriorityClass ProcessPriorityClass = ProcessPriorityClass.Normal;
        /// <summary>
        /// Boolean for Ignore of ProcessPriorityClass AND the process at all, Default is True!
        /// </summary>
        [DataMember]
        public bool Ignore = true;
        /// <summary>
        /// Boolean for Ignoring Thread Priority setting, Default is True!
        /// </summary>
        [DataMember]
        public bool IgnoreForcingThreadPriority = true;
        /// <summary>
        /// ThreadPriorityLevel for all ProcessThreads see msdn https://docs.microsoft.com/de-de/dotnet/api/system.diagnostics.threadprioritylevel?view=netframework-4.8
        /// </summary>
        [DataMember]
        public ThreadPriorityLevel ThreadPriorityLevel = ThreadPriorityLevel.Normal;
        /// <summary>
        /// Boolean for Kill the process on sight if <ignore>true</ignore>, default is false!
        /// </summary>
        [DataMember]
        public bool KillProcess = false;
        /// <summary>
        /// Explicit Interface Implementation for use in Cache
        /// </summary>
        [IgnoreDataMember]
        string ICacheFile.Key => Name;
    }
}