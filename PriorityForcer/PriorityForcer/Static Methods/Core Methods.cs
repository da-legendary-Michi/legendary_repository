﻿using System;
using System.Security.Principal;
namespace PriorityForcer.Static_Methods
{
    /// <summary>
    /// Place for static Core_Methods
    /// </summary>
    internal static class Core_Methods
    {
        /// <summary>
        /// checks if User has admin rights in Userinstance
        /// </summary>
        /// <returns></returns>
        public static bool IsUserAdministrator()
        {
            bool isAdmin;
            try
            {
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException)
            {
                isAdmin = false;
            }
            catch (Exception)
            {
                isAdmin = false;
            }
            return isAdmin;
        }
    }
}
