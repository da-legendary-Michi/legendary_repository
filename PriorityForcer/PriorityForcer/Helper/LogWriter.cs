﻿using System;
using System.IO;
using System.Reflection;

namespace PriorityForcer.Helper
{
    /// <summary>
    /// Simple Logwriter, adjusted and corrected from a StackOverflow Solution
    /// </summary>
    public static class LogWriter
    {
        private static string excecutionPath = string.Empty;
        /// <summary>
        /// Creates Streamwriter for Logfile
        /// </summary>
        /// <param name="logMessage">Stringcontent of Logmessage</param>
        public static void LogWrite(string logMessage)
        {
            excecutionPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            try
            {
                using (StreamWriter w = File.AppendText(excecutionPath + "\\" + $"{DateTime.Now.ToShortDateString()}" + "log.txt"))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception)
            {
            }
        }
        /// <summary>
        /// Concrete Log function intentional for LogWriter
        /// </summary>
        /// <param name="logMessage">Stringcontent of Logmessage</param>
        /// <param name="txtWriter">StreamWriter/TextWriter</param>
        public static void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\r\nLog Entry : ");
                txtWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                txtWriter.WriteLine("  :{0}", logMessage);
                txtWriter.WriteLine("-------------------------------");
            }
            catch (Exception)
            {
                
            }
        }
    }
}
