## Intro Why I created PriorityForcer
I run a Razer Laptop and play with an external graphics card, it has only a dual core cpu however, the cpu itself is still good. 
Everytime I joined in a CSGO server, I saw in taskmanger that steamservice, steamwebhelper and steam itself used between 20-30% gpu usage. 
So I started opening my taskmanager manually (because I found no tool which served this exact purpose) located the details tab 
and switched Priority to low on all of these programs. I instantly got more FPS and performance but after I joined a different server 
or 20-30 Minutes passed, the steam services had automatically been set again to High Priority!

After weeks of doing this, I decided to create this tool to "force" any appliation I want in the ProcessPriority and ProcessThreadPriority to whatever priority I wanted it to be.

### Requirement
.NET-Framework 4.8 (last .NET-Framework with old Service structure)

### Environment
- Works as ConsoleAPP or Service
 - You can set it up (services on Windows need to be already set up) (and to debug your config run it as ConsoleAPP) OR
 - You can let it just run as ConsoleAPP in a directory

### Installation
# Pull this Git.
> https://bitbucket.org/da-legendary-Michi/legendary_repository/downloads/  (or pull and build)

# Start from Release-Directory or Start Setup from Setup Release-Directory
> PriorityForcer: https://bitbucket.org/da-legendary-Michi/legendary_repository/src/master/PriorityForcer/PriorityForcer/bin/ 
> PriorityForcerSetup: https://bitbucket.org/da-legendary-Michi/legendary_repository/src/master/PriorityForcer/PriorityForcerSetup/

# Configure config.xml
> If you installed it, it should be placed "C:\Program Files\Michi\PriorityForcer\config.xml" here.

### What does it???
1. After start -> Listing all active services and Add's them (if not existant) to config.xml.
2. It controls if processes run you want to force a priority to and runs every x milliseconds (->config.xml setting)
3. The Threads (because of the far greater number) get cached and processed delayed by backgroundThreads.
4. Or it kills the process u want to be killed while the PriorityForcer runs.


### Configure It
> I recommend using notepad++ or similar Editors for configuring config.xml

** If not existant, it creates a config.xml in Execution-Directory (Program Directory). 
The config-File has a "ProcessList" and a "SettingConfig". In SettingConfig you define the amount of milliseconds between each run of PriorityForcer. **

```
  <SettingConfig>
    <pollTimeInMilliseconds>300000</pollTimeInMilliseconds>
```
You can also DisableLogging or just enable Exception-Logging

```
	<DisableLogging>false</DisableLogging>
    <OnlyExceptionLogging>false</OnlyExceptionLogging>
```

** In ProcessList is every (by user rights of PriorityForcer accessible) process found. 
If you change the value of the "trigger" <Ignore>false</Ignore> PriorityForcer looks 
every run for this service and if the ProcessPriorityClass matches your wanted value. **

> possible values for ProcessPriorityClass are here: https://docs.microsoft.com/de-de/dotnet/api/system.diagnostics.processpriorityclass?view=netframework-4.8

``` 
	ProcessPriorityClass:
	
	AboveNormal
	BelowNormal
	High
	Idle
	Normal
	RealTime
```

** If you set IgnoreForcingThreadPriority to false <IgnoreForcingThreadPriority>false</IgnoreForcingThreadPriority>
 than a Background-Thread looks for every Thread of processes with <Ignore>false</Ignore> 
 and if the ThreadPriorityLevel differs from the one in your config it forces your wanted ThreadPriorityLevel. **

** If you set in a <ProcessConfig> **
1. from <KillProcess>false</KillProcess> and <Ignore>false</Ignore> 
2. to <KillProcess>true</KillProcess> and <Ignore>true</Ignore> 
**than PriorityForcer tries to Kill this process (and every sub process)**

> possible values for ThreadPriorityLevel are here: https://docs.microsoft.com/de-de/dotnet/api/system.diagnostics.threadprioritylevel?view=netframework-4.8
 
``` 
	ThreadPriorityLevel:
	
	AboveNormal
	BelowNormal
	Highest
	Idle
	Lowest
	Normal
	TimeCritical
```
```
 In the config part of <ProcessList> below we have two Process entries.
 The First ProcessConfig entry belongs to Taskmgr and because 
      <Ignore>true</Ignore>
      <IgnoreForcingThreadPriority>true</IgnoreForcingThreadPriority>
 Taskmgr is ignored by PriorityForcer.
 
 The Second ProcessConfig entry belongs to steamservice.
 <ProcessPriorityClass>BelowNormal</ProcessPriorityClass>
 and
 <ThreadPriorityLevel>Lowest</ThreadPriorityLevel>
 gets forced if the found process and the processthread priorities differ from your config.xml entry.
 
  <ProcessList>
    <ProcessConfig>
      <Name>Taskmgr</Name>
      <ProcessPriorityClass>Normal</ProcessPriorityClass>
      <Ignore>true</Ignore>
      <IgnoreForcingThreadPriority>true</IgnoreForcingThreadPriority>
      <ThreadPriorityLevel>Normal</ThreadPriorityLevel>
	  <KillProcess>false</KillProcess>
    </ProcessConfig>
    <ProcessConfig>
      <Name>steamservice</Name>
      <ProcessPriorityClass>BelowNormal</ProcessPriorityClass>
      <Ignore>false</Ignore>
      <IgnoreForcingThreadPriority>false</IgnoreForcingThreadPriority>
      <ThreadPriorityLevel>Lowest</ThreadPriorityLevel>
	  <KillProcess>false</KillProcess>
    </ProcessConfig>
```

### Error-Log an Debug
1. in Program-Directory "C:\Program Files\Michi\PriorityForcer\18.02.2022log.txt"
2. As ConsoleAPP PriorityForcer displays every step it does in console. You can disable this feature in the SettingConfig part of config.xml